#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include <QGraphicsScene>
#include <QGraphicsBlurEffect>
#include <QGraphicsPixmapItem>
#include <QPainter>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

QImage MainWindow::blurImage(QImage source, int blurRadius)
{
    if(source.isNull()) return QImage();
    QGraphicsScene scene;
    QGraphicsPixmapItem item;
    item.setPixmap(QPixmap::fromImage(source));

    auto *blur = new QGraphicsBlurEffect;
    blur->setBlurRadius(blurRadius);
    item.setGraphicsEffect(blur);
    scene.addItem(&item);
    QImage result(source.size(), QImage::Format_ARGB32);
    result.fill(Qt::transparent);
    QPainter painter(&result);
    scene.render(&painter, QRectF(), QRectF(0,0, source.width(), source.height()));

    return result;
}

void MainWindow::slot1()
{
    if(filePath != "")
    {
        ui->label->setPixmap(QPixmap::fromImage(MainWindow::blurImage(QImage(filePath),  ui->horizontalSlider->value()).scaled( ui->label->width(), ui->label->height(), Qt::KeepAspectRatio)));
    }
};

void MainWindow::slot2()
{
    filePath = QFileDialog::getOpenFileName(nullptr, "Open Picture JPG", "./", "jpg-files (*.jpg)");
    ui->label->setPixmap(QPixmap::fromImage(MainWindow::blurImage(QImage(filePath),  0).scaled( ui->label->width(), ui->label->height(), Qt::KeepAspectRatio)));
    ui->label->update();
    ui->horizontalSlider->setValue(0);
    ui->horizontalSlider->update();
};

